//
// Created by eugen on 08/02/19.
//

#ifndef SEG_GRAPH_H
#define SEG_GRAPH_H

#include <opencv2/opencv.hpp>
#include <ostream>
#include <unordered_set>
#include "Edge.h"
#include "VertexHasher.h"

using namespace std;
using namespace cv;

class Graph {
public:

    int width, height;
    Vertex source;
    Vertex sink;
    Mat *const imgPtr;
    unordered_map<Vertex, vector<Edge>, VertexHasher, VertexEqual> adj_list;

    explicit Graph(Mat *, Vertex, Vertex);

    virtual ~Graph();

    void populate_graph();

    void connect_s_t(unordered_map<Vertex, vector<Vertex>, VertexHasher, VertexEqual>);

    double pixels_difference(const Vertex &, const Vertex &);

    void add_edge(const Vertex &, const Vertex &);

    friend ostream &operator<<(ostream &, const Graph &);

};


#endif //SEG_GRAPH_H
