//
// Created by eugen on 08/02/19.
//

#ifndef SEG_EDGE_H
#define SEG_EDGE_H

#include <ostream>
#include <opencv2/opencv.hpp>
#include "Vertex.h"

using namespace std;
using namespace cv;

class Edge {
    friend class ResidualGraph;

public:
    Vertex from;
    Vertex to;
    double cap;
    double fl = 0.0;

    Edge();

    Edge(Vertex, Vertex, double);

    virtual ~Edge();

    Vertex get_opposite_vertex_for(const Vertex &) const;

    double residual_capacity_to(const Vertex &vertex) const;

    void change_flow_to(const Vertex &, double);

    bool operator==(const Edge &) const;

    bool operator!=(const Edge &) const;

    friend ostream &operator<<(ostream &, const Edge &);

};


#endif //SEG_EDGE_H
