//
// Created by eugen on 25/02/19.
//

#ifndef SEG_VERTEXHASHER_H
#define SEG_VERTEXHASHER_H

#include <cstddef>
#include "Vertex.h"

/**
 * Custom hashing function to use the Vertex as a key in unordered_map collection
 */
class VertexHasher {
public:
    unsigned long long operator()(const Vertex k) const {
        return ((unsigned long long) k.col * 97U +
                k.row);  // the larger the unsigned prime number, the faster retrieval, though the more space needed
//        return std::hash<unsigned long long>()((unsigned long long) k.col * 97U + k.row); // slower
    }
};

/**
 * Custom equality function to in unordered_map collection having Vertex as a key
 */
class VertexEqual {
public:
    bool operator()(Vertex const &v1, Vertex const &v2) const {
        return v1.col == v2.col &&
               v1.row == v2.row;
    }
};

#endif //SEG_VERTEXHASHER_H
