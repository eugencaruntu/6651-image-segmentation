//
// Created by eugen on 08/02/19.
//

#include "Edge.h"
#include "Vertex.h"

/**
 * Parameterized ctor. Flow will be instantiated to 0
 * @param from vertex
 * @param to vertex
 * @param cap the capacity of the edge
 */
Edge::Edge(Vertex from, Vertex to, double cap) : from(from), to(to), cap(cap) {}

Edge::Edge() = default;

/**
 * Compare the edge
 * @param rhs edge
 * @return true if all attributes match, false otherwise
 */
bool Edge::operator==(const Edge &rhs) const {
    return from == rhs.from &&
           to == rhs.to &&
           cap == rhs.cap &&
           fl == rhs.fl;
}

bool Edge::operator!=(const Edge &rhs) const {
    return !(rhs == *this);
}

/**
 * Get the other vertex of an edge given one of the vertices
 * @param vertex the given vertex of the edge
 * @return the other vertex of the edge
 */
Vertex Edge::get_opposite_vertex_for(const Vertex &vertex) const {
    Vertex opposite_vertex;
    // if from return to
    if (vertex == from) {
        opposite_vertex = to;
        // if to return from
    } else if (vertex == to) {
        opposite_vertex = from;
        // to make sure we always pass the correct vertex (making sure we had set the edges properly)
    } else {
        throw "Wrong vertex/pixel passed. Can't return the get_opposite_vertex_for vertex";
    }
    return opposite_vertex;
}

/**
 * Get the residual capacity of an edge.
 * If is an incoming edge we return the flow
 * If it is an outgoing edge we return the capacity minus flow
 * @param vertex the given vertex
 * @return the residual value to be considered
 */
double Edge::residual_capacity_to(const Vertex &vertex) const {
    double residual_capacity;
    // when the edge is incoming from <-- to
    if (vertex == from) {
        residual_capacity = fl;
    }
        // when the edge is outgoing from --> to
    else if (vertex == to) {
        residual_capacity = cap - fl;
    }
        // if wrong vertex is passed (edges were not set correctly in the graph)
    else {
        throw "Wrong vertex/pixel passed. Can't return the capacity to vertex";
    }
    return residual_capacity;
}

/**
 * Update the flow of an edge given a vertex and a value.
 * If is an incoming edge we substract the value from the flow
 * If it is an outgoing edge we add the value to the flow
 * @param vertex
 * @param value
 */
void Edge::change_flow_to(const Vertex &vertex, double value) {
    // if the edge is incoming from <-- to
    if (vertex == from) {
        fl -= value;
    }
        // if the edge is outgoing from --> to
    else if (vertex == to) {
        fl += value;
    }
        // if wrong vertex is passed (edges were not set correctly in the graph)
    else {
        throw "Wrong vertex/pixel passed. Can't change the capacity";
    }
}

/**
 * Stream for an edge object
 * @param os
 * @param edge
 * @return streamed edge
 */
ostream &operator<<(ostream &os, const Edge &edge) {
    os << edge.from << "> " << edge.fl << "/" << edge.cap << " >" << edge.to;
    return os;
}

/**
 * Dtor
 */
Edge::~Edge() = default;
