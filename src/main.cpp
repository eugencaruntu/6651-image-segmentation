/*
 * Image segmentation using Maxlow / MinCut implementation of Ford-Fulkerson algorithm and opencv
 * Eugen Caruntu #29077103
 */

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "Graph.h"
#include "ResidualGraph.h"
#include <chrono>

using namespace std;
using namespace cv;
using namespace std::chrono;

int main(int argc, char **argv) {

    // start the clock
//    high_resolution_clock::time_point start_time = high_resolution_clock::now();

    if (argc != 4) {
        cout << "Usage: ../seg input_image initialization_file output_mask" << endl;
        return -1;
    }

    // Load the input image
    Mat in_image;
    in_image = imread(argv[1] /*, CV_LOAD_IMAGE_COLOR*/);

    if (!in_image.data) {
        cout << "Could not load input image!!!" << endl;
        return -1;
    }

    if (in_image.channels() != 3) {
        cout << "Image does not have 3 channels!!! " << in_image.depth() << endl;
        return -1;
    }

    // the output image
    Mat out_image = in_image.clone();

    ifstream f(argv[2]);
    if (!f) {
        cout << "Could not load initial mask file!!!" << endl;
        return -1;
    }

    int width = in_image.cols;
    int height = in_image.rows;

    int n;
    f >> n;

    // Convert the image to L*a*b* color space for more reliable segmentation (L: brightness, a: red-green axis, b: blue-yellow axis)
    Mat lab_image;
    cvtColor(in_image, lab_image, COLOR_BGR2Lab);

    // Apply Gaussian Blur on a copy of image before computing weights to reduce the noise
    // The optimal size of the kernel was set based on tests
    Mat blured;
    GaussianBlur(lab_image, blured, Size(3, 3), 0, BORDER_DEFAULT);

    // Mock-up Vertex objects for source and sink
    Vertex source{width + 1, height + 1};
    Vertex sink{width + 2, height + 2};

    // Collection to store the pixels marked in input config
    unordered_map<Vertex, vector<Vertex>, VertexHasher, VertexEqual> marked_pixels;

    // Get the initial pixels as marked and add them to the map
    for (int i = 0; i < n; ++i) {
        int x, y, t;
        f >> x >> y >> t;

        if (x < 0 || x >= width || y < 0 || y >= height) {
            cout << "I valid pixel mask!" << endl;
            return -1;
        }

        Vertex vertex{x, y};
        if (t == 1) {
            marked_pixels[source].push_back(vertex);
        } else {
            marked_pixels[sink].push_back(vertex);
        }

    }

    // Construct the graph on blured grayscale version
    Graph graph{&blured, source, sink};

    // Connect the source and sink
    graph.connect_s_t(marked_pixels);
//    cout << graph;

    // Make a residual graph
    ResidualGraph residualGraph{graph, source, sink};

    // Increase the flow until maximum flow is found
    residualGraph.increase_flow();
//    cout << "The maximum flow is: " << residualGraph.flow << endl;

    // Color the final image to shod segmentation
    Vec3b pixel;
    pixel[1] = 0;
    for (auto &entry: residualGraph.res_graph.adj_list) {
        Vertex vertex = entry.first;
        if (vertex == source || vertex == sink) continue;
        if (residualGraph.reacheable_from_source(vertex)) {
            pixel[0] = 0;
            pixel[2] = 255;
            out_image.at<Vec3b>(vertex.row, vertex.col) = pixel;
        } else {
            pixel[0] = 255;
            pixel[2] = 0;
            out_image.at<Vec3b>(vertex.row, vertex.col) = pixel;
        }
    }

    /*
      // display the runtime
      high_resolution_clock::time_point end_time = high_resolution_clock::now();
      auto run_time = duration_cast<seconds>(end_time - start_time).count();
      cout << "Execution time: " << run_time << " sec" << endl;
    */

    // write it on disk
    imwrite(argv[3], out_image);

    // also display them both
    namedWindow("Original image", WINDOW_AUTOSIZE);
    namedWindow("Image Segmentation", WINDOW_AUTOSIZE);
    imshow("Original image", in_image);
    imshow("Image Segmentation", out_image);

    waitKey(0);
    return 0;

}
