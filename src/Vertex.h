//
// Created by eugen on 24/02/19.
//

#ifndef SEG_VERTEX_H
#define SEG_VERTEX_H


#include <opencv2/opencv.hpp>
#include <ostream>

using namespace cv;
using namespace std;

class Vertex {

public:

    int col;
    int row;

    Vertex();

    explicit Vertex(int, int);

    virtual ~Vertex();

    bool operator==(const Vertex &) const;

    bool operator!=(const Vertex &) const;

    friend ostream &operator<<(ostream &, const Vertex &);

};

#endif //SEG_VERTEX_H
