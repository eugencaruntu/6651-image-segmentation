//
// Created by eugen on 08/02/19.
//

#include "Graph.h"

using namespace std;
using namespace cv;

/**
 * Parameterized ctor
 * @param imgPtr a pointer to the image matrix
 * @param source vertex
 * @param sink vertex
 */
Graph::Graph(cv::Mat *imgPtr, Vertex source, Vertex sink) : imgPtr(imgPtr), source(source), sink(sink) {
    width = imgPtr->cols;
    height = imgPtr->rows;
    populate_graph();
}

/**
 * Put all the pixels and capacities in the graph by iterating once over the image
 */
void Graph::populate_graph() {

    // We iterate over all pixels of the image
    for (int r = 0; r < height; r++) {

        for (int c = 0; c < width; c++) {
            // current pixel
            Vertex current_pixel{c, r};

//            // up WE DO NOT NEED TO ADD THIS SINCE WE DO THAT WHEN WE BUILD THE EDGE
//            if (r > 0) {
//                Vertex up_pixel{c, r - 1};
//                add_edge(current_pixel, up_pixel);
//            }

            // right
            if (c < width - 1) {
                Vertex right_pixel{c + 1, r};
                add_edge(current_pixel, right_pixel);
            }

            // down
            if (r < height - 1) {
                Vertex down_pixel{c, r + 1};
                add_edge(current_pixel, down_pixel);
            }

//            // left WE DO NOT NEED TO ADD THIS SINCE WE DO THAT WHEN WE BUILD THE EDGE
//            if (c > 0) {
//                Vertex left_pixel{c - 1, r};
//                add_edge(current_pixel, left_pixel);
//            }
        }
    }
}

/**
 * Add an edge to both pixel's adjacency list.
 * !! We only traverse the image right/down so we add the edge on both vertices !!
 * The capacity is computed calling the pixels_difference() function
 * @param from_vertex
 * @param to_vertex
 */
void Graph::add_edge(const Vertex &from_vertex, const Vertex &to_vertex) {
    adj_list[from_vertex].push_back(Edge{from_vertex, to_vertex, pixels_difference(from_vertex, to_vertex)});
    adj_list[to_vertex].push_back(Edge{to_vertex, from_vertex, pixels_difference(from_vertex, to_vertex)});
}

/**
 * Connect the marked pixels to source or target
 * @param marked_pixels is a map holding source and sink pixels and their mapped vector of pixel coordinates to be connected with
 */
void Graph::connect_s_t(unordered_map<Vertex, vector<Vertex>, VertexHasher, VertexEqual> marked_pixels) {
    for (Vertex &vertex: marked_pixels[source]) {
        adj_list[source].push_back(Edge{source, vertex, numeric_limits<double>::infinity()});
        adj_list[vertex].push_back(Edge{vertex, source, numeric_limits<double>::infinity()});
    }
    for (Vertex &vertex: marked_pixels[sink]) {
        adj_list[vertex].push_back(Edge{vertex, sink, numeric_limits<double>::infinity()});
        adj_list[sink].push_back(Edge{sink, vertex, numeric_limits<double>::infinity()});
    }
}

/**
 * Calculate the difference between two pixels for the image.
 * Method assumes 3 channel image (L*a*b is desired)
 * This implementation follows in part the CIE76 delta E, but the formula is derived so that it maximizes the dissimilarities between pixels.
 * A similar approach: Eriksson, A. P., Barr, O., & Astrom, K. (2006). Image segmentation using minimal graph cuts.
 * The adjustment factor is optimized for speed at the expense of small edge imperfections when blurring was applied.
 * @param vertex_1
 * @param vertex_2
 * @return the di-similarity between pixels
 */

double Graph::pixels_difference(const Vertex &vertex_1, const Vertex &vertex_2) {
    double difference = 0;
    // larger adjustment might be faster, but needs testing on other images edge can be jagged (tested on vultur.jpg with correct output)
    int adjustment = 10;

    Vec3b pixel_1_vals = imgPtr->at<Vec3b>(vertex_1.row, vertex_1.col);
    Vec3b pixel_2_vals = imgPtr->at<Vec3b>(vertex_2.row, vertex_2.col);
    for (int i = 0; i < 3; i++) {
        difference += abs((pixel_1_vals[i] - pixel_2_vals[i]));
    }
    return exp(-adjustment * pow(difference, 2));
}


/**
 * Show the graph
 * @param os the stream
 * @param graph the graph to print
 * @return the streamed graph
 */
ostream &operator<<(ostream &os, const Graph &graph) {
    for (const auto &entry :graph.adj_list) {
        os << entry.first;
        for (const Edge &edge : entry.second) {
            os << "\t" << edge;
        }
        os << endl;
    }
    return os;
}

/**
 * Dtor
 */
Graph::~Graph() = default;

