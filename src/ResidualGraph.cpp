//
// Created by eugen on 22/02/19.
//

#include "Graph.h"
#include "ResidualGraph.h"

/**
 * Parameterized ctor
 * @param graph
 * @param source vertex
 * @param sink vertex
 */
ResidualGraph::ResidualGraph(Graph graph, Vertex source, Vertex sink) : res_graph(graph),
                                                                        source(source),
                                                                        sink(sink) {
    flow = 0.0; // the initial max flow for the residual graph
}

/**
 * Using BFS, determine if there is an augumenting path from source to sink
 * At the same time, we keep track of the nodes that were reached from the source.
 * This bookkeeping will let us determine which nodes belong in the cut
 * @return true if sink is part of the visited vertices, false otherwise
 */
bool ResidualGraph::bfs() {

    // instantiate an boolean map to keep track of visited vertices along the explored path
    for (auto &entry: res_graph.adj_list) {
        visited[entry.first] = false;
    }

    // the que of children to visit for BFS
    queue<Vertex> children;
    children.push(source);
    visited[source] = true;

    while (!children.empty()) {
        // explore the vertex at the front of the queue
        Vertex vertex = children.front();
        children.pop();

        // for each of the edges in the adjacency vector for current vertex
        for (Edge &edge: res_graph.adj_list[vertex]) {
            // check the oposite vertex on that edge
            Vertex other_vertex = edge.get_opposite_vertex_for(vertex);
            // if not already visited and has capacity > 0
            if (!visited[other_vertex] && edge.residual_capacity_to(other_vertex) > 0) {
                // mark the vertex as visited along the explored path
                visited[other_vertex] = true;
                // mark the last edge that reached the vertex so we know where to change the flow if needed
                last_edge_to[other_vertex] = &edge;
                // add the other vertex on this edge to the queue so we visit it later
                children.push(other_vertex);
            }
        }
    }

    // return if we reached the sink
    return visited[sink];
}

/**
 * Increase the flow as long as we find an augumenting path
 * Once we found an augumenting poath, we determine the smallest capacity on it
 * Then we update the flow along this path using the smallest capacity found
 */
void ResidualGraph::increase_flow() {
    // as long we have a path that connects source to sink
    while (bfs()) {
        // assume infinite capacity is possible
        double smallest = numeric_limits<double>::infinity();

        // find the edge with smallest flow across entire path from sink to source
        Vertex vertex = sink; // start from sink because there is an edge towards sink
        while (vertex != source) {  // as long as we do not reach the source
            smallest = min(smallest, last_edge_to[vertex]->residual_capacity_to(vertex));
            // set the current vertex to the last one that brought us to it
            vertex = last_edge_to[vertex]->get_opposite_vertex_for(vertex);
        }
//        cout << "bottleneck capacity is: " << smallest << endl;

        // recalculate the flow for the path that brought us from sink to source
        vertex = sink;  // make sure we are at sink again
        while (vertex != source) {  // as long as we do not reach source, change the flow
            last_edge_to[vertex]->change_flow_to(vertex, smallest);
            // set the current vertex to the last one that brought us to it
            vertex = last_edge_to[vertex]->get_opposite_vertex_for(vertex);
        }
        flow += smallest;
//        cout << res_graph;
//        cout << "FLOW: " << flow << endl;
    }
}

/**
 * Helper method to indicate if a vertex is part of the cut
 * @param vertex
 * @return true if the vertex was visited in the last BFS iteration, false otherwise
 */
bool ResidualGraph::reacheable_from_source(Vertex vertex) {
    return visited[vertex];
}

/**
 * Dtor
 */
ResidualGraph::~ResidualGraph() = default;


