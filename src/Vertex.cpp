
//
// Created by eugen on 24/02/19.
//

#include "Vertex.h"

/**
 * Parameterized ctor
 * Creates a vertex with the pixel coordinates from the image matrix
 * @param col the column index
 * @param row the row index
 */
Vertex::Vertex(int col, int row) : col(col), row(row) {}

Vertex::Vertex() = default;

/**
 * Stream for vertex
 * @param os
 * @param vertex
 * @return streamed vertex
 */
ostream &operator<<(ostream &os, const Vertex &vertex) {
    os << "(" << vertex.col << "," << vertex.row << ")";
    return os;
}

/**
 * Equality operator
 * @param rhs the other vertex
 * @return true if coordinates match
 */
bool Vertex::operator==(const Vertex &rhs) const {
    return col == rhs.col &&
           row == rhs.row;
}


bool Vertex::operator!=(const Vertex &rhs) const {
    return !(rhs == *this);
}

/**
 * Dtor
 */
Vertex::~Vertex() = default;
