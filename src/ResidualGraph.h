//
// Created by eugen on 22/02/19.
//

#ifndef SEG_RESIDUALGRAPH_H
#define SEG_RESIDUALGRAPH_H

#include "Edge.h"
#include "Graph.h"
#include "VertexHasher.h"

class ResidualGraph {
public:
    Graph res_graph;
    Vertex source;
    Vertex sink;
    double flow;
    unordered_map<Vertex, Edge *, VertexHasher, VertexEqual> last_edge_to;
    unordered_map<Vertex, bool, VertexHasher, VertexEqual> visited;

    ResidualGraph(Graph, Vertex, Vertex);

    virtual ~ResidualGraph();

    bool bfs();

    void increase_flow();

    bool reacheable_from_source(Vertex);

};

#endif //SEG_RESIDUALGRAPH_H
